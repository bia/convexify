package plugins.adufour.roi;

import icy.roi.ROI;
import icy.roi.ROI2D;
import icy.roi.ROI3D;
import icy.roi.ROIUtil;
import icy.sequence.Sequence;
import icy.type.point.Point3D;

import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.List;

import javax.vecmath.Point3d;

import plugins.adufour.blocks.tools.roi.ROIBlock;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.quickhull.QuickHull2D;
import plugins.adufour.quickhull.QuickHull3D;
import plugins.adufour.roi.mesh.polygon.ROI3DPolygonalMesh;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.kernel.roi.roi2d.ROI2DArea;
import plugins.kernel.roi.roi2d.ROI2DPolygon;
import plugins.kernel.roi.roi2d.ROI2DShape;
import plugins.kernel.roi.roi3d.ROI3DArea;

public class Convexify extends EzPlug implements ROIBlock
{
    private EzVarSequence input = new EzVarSequence("Input sequence");

    private EzVarBoolean replace = new EzVarBoolean("Replace existing ROI", false);

    private VarROIArray roiIN = new VarROIArray("List of ROI");

    private VarROIArray roiOUT = new VarROIArray("List of ROI");

    @Override
    protected void initialize()
    {
        addEzComponent(input);
        addEzComponent(replace);
    }

    @Override
    public void clean()
    {

    }

    @Override
    public void execute()
    {
        if (!isHeadLess())
        {
            roiIN.setValue(new ROI[0]);
            Sequence s = input.getValue(true);
            roiIN.add(s.getROIs().toArray(new ROI[0]));
        }

        roiOUT.setValue(new ROI[0]);
        for (ROI roi : roiIN)
            roiOUT.add(createConvexROI(roi));

        if (!isHeadLess())
        {
            Sequence s = input.getValue(true);

            if (replace.getValue())
                s.removeAllROI();

            for (ROI roi : roiOUT)
            {
                s.addROI(roi);
            }
        }
    }

    /**
     * Creates a convex ROI from the specified ROI.
     * 
     * @param roi
     *        the ROI to be converted into a convex version
     * @return a convex version of the ROI. This ROI is currently of type {@link ROI2DPolygon} in
     *         2D, and of type {@link ROI3DArea} in 3D, although the return types may change in the
     *         future.
     * @throws IllegalArgumentException
     *         if the specified ROI is not supported
     */
    public static ROI createConvexROI(ROI roi) throws IllegalArgumentException
    {
        ROI output = null;

        try
        {
            if (roi instanceof ROI2D)
            {
                final List<Point2D> envelope;

                if (roi instanceof ROI2DShape)
                {
                    envelope = QuickHull2D.computeConvexEnvelope(((ROI2DShape) roi).getPoints());
                }
                else if (roi instanceof ROI2DArea)
                {
                    Point2D[] points = ((ROI2DArea) roi).getBooleanMask(true).getContourPoints();
                    envelope = QuickHull2D.computeConvexEnvelope(Arrays.asList(points));
                }
                else
                {
                    throw new IllegalArgumentException("Cannot compute convex hull for ROI " + roi.getName()
                            + " of type " + roi.getClassName() + ".");
                }

                output = new ROI2DPolygon(envelope);

                // copy position info
                final ROI2D roi2d = (ROI2D) roi;
                ((ROI2D) output).setT(roi2d.getT());
                ((ROI2D) output).setZ(roi2d.getZ());
                ((ROI2D) output).setC(roi2d.getC());
            }
            else if (roi instanceof ROI3D)
            {
                Point3D[] points = ((ROI3D) roi).getBooleanMask(true).getContourPoints();

                // convert to vecmath's Point3d
                Point3d[] pt3d = new Point3d[points.length];
                for (int i = 0; i < points.length; i++)
                {
                    Point3D p = points[i];
                    pt3d[i] = new Point3d(p.getX(), p.getY(), p.getZ());
                }

                QuickHull3D qhull = new QuickHull3D();
                qhull.build(pt3d, pt3d.length);

                // Use a 3D mesh to prepare the final ROI
                output = new ROI3DPolygonalMesh(qhull);

                // copy position info
                final ROI3D roi3d = (ROI3D) roi;
                ((ROI3D)output).setT(roi3d.getT());
                ((ROI3D)output).setC(roi3d.getC());
            }
        }
        catch (Throwable t)
        {
            // can happen with QuickHull3D / ROI3DPolygonalMesh...
            throw new IllegalArgumentException(
                    "Cannot compute convex hull for ROI " + roi.getName() + " of type " + roi.getClassName() + ".", t);
        }

        if (output == null)
            throw new IllegalArgumentException(
                    "Cannot compute convex hull for ROI " + roi.getName() + " of type " + roi.getClassName() + ".");

        ROIUtil.copyROIProperties(roi, output, false);
        output.setName(roi.getName() + " (convex)");

        return output;
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("Regions of interest", roiIN);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("Regions of interest", roiOUT);
    }
}
